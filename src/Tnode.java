import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode(String name){
      this.name = name;
      firstChild = null;
      nextSibling = null;
   }

   Tnode(){
   }

   private  void  dfs(StringBuffer ans, Tnode t){
      if (t == null) {
         return;
      }
      ans.append(t.name);
      if (t.firstChild!=null){
         if (t.firstChild.nextSibling!= null){
            if (t.firstChild != null) {

               dfs(ans.append('('), t.firstChild.nextSibling);
               dfs(ans.append(','), t.firstChild);
               ans.append(')');
            } else if (t.firstChild.nextSibling != null) {
               dfs(ans.append('('), t.firstChild.nextSibling);
               ans.append(')');
            }
         }
      }

   }


   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      Tnode builder = this;
      dfs(b,builder);
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<String> tokens = toTokens(pol);
      return buildTree(tokens);
   }

   private static Tnode buildTree(Stack<String> tokens){
      Tnode root = new Tnode();
      Tnode clone = root;

      root.name = tokens.pop();
      if (!tokens.empty()){
         if (isOperator(tokens.peek())){
            root.firstChild = buildTree(tokens); //basically left
         }else {
            root.firstChild = new Tnode(tokens.pop()); //basically left
         }

      }
      if (!tokens.empty()){
         if (isOperator(tokens.peek())){
            root.firstChild.nextSibling = buildTree(tokens); //basically right
         }else {
            root.firstChild.nextSibling = new Tnode(tokens.pop());
         }
      }
      return root;
   }

   private static boolean isOperator(String val){
      return val.equals("+") || val.equals("-") || val.equals("/") || val.equals("*") || val.equals("SWAP");
   }

   private static boolean isNumber(String val){
      Integer test = Integer.parseInt(val);
      return true;
   }


   private static Stack<String> toTokens(String pol){
      StringTokenizer tokenizer = new StringTokenizer(pol);
      Stack<String> tokens = new Stack<String>();
      String token = new String();
      int counter = 0;
      int numberCount=0;
      while (tokenizer.hasMoreTokens()){
         token = tokenizer.nextToken();

         if (token.equals("SWAP")){
            swap(tokens,pol);
            if (tokenizer.hasMoreTokens()){
               token = tokenizer.nextToken();
            }else{
               break;
            }
         }
         if (token.equals("ROT")){
            rot(tokens,pol);
            if (tokenizer.hasMoreTokens()){
               token = tokenizer.nextToken();
            }else{
               break;
            }
         }
         if (!isOperator(token)){
            isNumber(token);
            numberCount++;
         }
         tokens.push(token);
         counter++;
      }

      if (counter<3 && counter!=1){
         throw new RuntimeException("Insufficient number of symbols in expression: "+pol+" .");
      }else {
         if (counter == 1){
            if (isOperator(tokens.peek()) || !isNumber(tokens.peek())){
               throw new RuntimeException("Illegal symbol "+pol+" for building a one-element tree");
            }
         }else {
            if((counter-1)/(numberCount-1) != 2 ){
               throw new RuntimeException("Improper number of arguments in expression: "+pol+".");
            }
         }

      }
      return tokens;
   }


   private static void swap(Stack<String> tokens, String pol){
      String errorString = "Not enough elements for SWAP operation in expression " + pol;
      String a,b;
      if (!tokens.empty()){
          a = tokens.pop();
      }else {
         throw new RuntimeException(errorString);
      }
      if (!tokens.empty()){
          b = tokens.pop();
      }else{
         throw new RuntimeException(errorString);
      }

      tokens.push(a);
      tokens.push(b);
   }
   private static void rot(Stack<String> tokens, String pol){
      String errorString = "Not enough elements for ROT operation in expression " + pol;
      String a,b,c;
      if (!tokens.empty()){
          a = tokens.pop();
      }else{
         throw new RuntimeException(errorString);
      }
      if (!tokens.empty()){
          b = tokens.pop();
      }else{
         throw new RuntimeException(errorString);
      }
      if (!tokens.empty()){
          c = tokens.pop();
      }else{
         throw new RuntimeException(errorString);
      }

      tokens.push(a);
      tokens.push(c);
      tokens.push(b);
   }

   public static void main (String[] param) {
      String rpn = "2 5 9 ROT + SWAP -";
      System.out.println ("RPN: " + rpn);

      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
   }
}

